/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otimiz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Otimiz {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            File file = new File("..//ccmd//ccmd07.ins"); //args[0]
            Scanner input = new Scanner(file);
            String entrada = input.nextLine();
            String[] strArray = entrada.split("\\s+");
            
            Graph G = new Graph();
            G.setVertex(Integer.parseInt(strArray[0]));
            G.setEdges(Integer.parseInt(strArray[1]));
           
            G.load(input);   
            
            G.bounds(G.findClick());
           

        } catch (IOException e) {
            System.out.println("Could not open the file!");
        }

    }

}
