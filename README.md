# Graph Clique Algorithm

-> Java:  JDK_12

Files:
01. ccmd07.ins
  Primeira linha: n m
  n = número de vértices
  m = número de arestas
  Demais linhas, uma por vértice: f g d n1 n2 n3 ... nd
      f = valor f do vértice
      g = valor g do vértice
      d = grau do vértice
      ni = número do i-ésime vizinho do vértice. Os vértices tem números 0,1,...,n-1.

02. Otimiz.java
03. Graph.java
04. Node.java

Encontrar o primeiro clique maior do que 2.
Encontre os valores de f/g e o elemento minimal e maximal

Find the first clique greater than 2.
compute the values f/g and the min and max element in the clique.

** ALGORITHM RESULT:
-> Vertex: 0
Clique elements: 
0 73 

 -> Vertex: 1
Clique elements: 
1 106 76 7 134 91 103 46 88 45 15 23 56 77 94 89 

Clique Values: 
1.2307692307692308 0.71875 0.7619047619047619 0.6790123456790124 1.1785714285714286 0.7676767676767676 0.6274509803921569 0.9710144927536232 1.0389610389610389 0.9090909090909091 1.4090909090909092 0.5257731958762887 1.290909090909091 0.9846153846153847 0.6811594202898551 1.180327868852459 
 
Min Bound: 0.5257731958762887 -> vertex: 23
 
Max Bound: 1.4090909090909092 -> vertex: 15

